// Object
const drink = {
  color: 'brow',
  carbonated: true,
  sugar: 40
};

type Drink = [string, boolean, number];

// array
// can swap order
const pepsi = ['brown', true, 40];
pepsi[0] = 40;
pepsi[1] = 'brown';

// tuple
const coke: [string, boolean, number] = ['brown', true, 40];
coke[0] = 40; // Type 'number' is not assignable to type 'string'.

// using type
const drPepper: Drink = ['brown', true, 40];
const sprite: Drink = ['clear', true, 40];
const tea: Drink = ['brown', false, 0];

// tuples only uses for csv file
const carSpecs: [number, number] = [400, 4454];

// object
const carStats = {
  horsepower: 400,
  weight: 3354
};